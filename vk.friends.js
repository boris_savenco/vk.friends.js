/**
 * vk.friends.js
 * by Pratnomenis
 *
 * v1.0
 * 16-10-2014
 *
 * = Array.prototype.indexOf & Function.prototype.bind
 * are hack's for IE <9
 * from: developer.mozilla.org
 *
 */

/**
 * Hack's for IE <9
 */
if (!Array.prototype.indexOf) {
  Array.prototype.indexOf = function (searchElement, fromIndex) {
    var k;
    if (this === null) {
      throw new TypeError('"this" is null or not defined');
    }
    var O = Object(this);
    var len = O.length >>> 0;
    if (len === 0) {
      return -1;
    }
    var n = +fromIndex || 0;
    if (Math.abs(n) === Infinity) {
      n = 0;
    }
    if (n >= len) {
      return -1;
    }
    k = Math.max(n >= 0 ? n : len - Math.abs(n), 0);
    while (k < len) {
      var kValue;
      if (k in O && O[k] === searchElement) {
        return k;
      }
      k++;
    }
    return -1;
  };
}
if (!Function.prototype.bind) {
  Function.prototype.bind = function (oThis) {
    if (typeof this !== 'function') {
      // ближайший аналог внутренней функции
      // IsCallable в ECMAScript 5
      throw new TypeError('Function.prototype.bind - what is trying to be bound is not callable');
    }
    var aArgs = Array.prototype.slice.call(arguments, 1),
      fToBind = this,
      fNOP = function () {},
      fBound = function () {
        return fToBind.apply(this instanceof fNOP && oThis ? this : oThis,
          aArgs.concat(Array.prototype.slice.call(arguments)));
      };
    fNOP.prototype = this.prototype;
    fBound.prototype = new fNOP();
    return fBound;
  };
}

/**
 * Sending post request and calculate it with callback
 * 
 * @param   {String}   url      url for request
 * @param   {Object}   data     data for request
 * @param   {Function} callback callback function
 */
function httpPost(url, data, callback) {
  var xmlhttp = getXmlHttp();
  xmlhttp.onreadystatechange = function () {
    if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
      var responce = JSON.parse(xmlhttp.responseText);
      callback(responce);
    }
  };
  xmlhttp.open('POST', url, true);
  xmlhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
  xmlhttp.send(data);

  function getXmlHttp() {
    var t;
    try {
      t = new ActiveXObject("Msxml2.XMLHTTP");
    } catch (e) {
      try {
        t = new ActiveXObject("Microsoft.XMLHTTP");
      } catch (c) {
        t = !1;
      }
    }
    return t || "undefined" === typeof XMLHttpRequest || (t = new XMLHttpRequest), t;
  }
}
/**
 * Add className
 * must be call with dom element that you want to modyfy
 * like classAdd.call(someDiv, 'someClassName')
 *
 * @param {string} clsName
 * @returns this
 */
function classAdd(clsName) {
  if (!clsName)
    return this;
  if (!this.className) {
    this.className = clsName;
  } else {
    var fullClass = this.className;
    var classList = fullClass.split(' ');
    if (classList.indexOf(clsName) !== -1)
      return this;
    classList.push(clsName);
    this.className = classList.join(' ').trim();
  }
  return this;
}
/**
 * Remove className
 * must be call with dom element that you want to modyfy
 * like classRemove.call(someDiv, 'someClassName')
 *
 * @param {string} clsName
 * @returns this
 */
function classRemove(clsName) {
  if (!clsName)
    return this;
  if (!this.className)
    return this;
  var fullClass = this.className;
  var rexp = new RegExp(clsName, 'gi');
  this.className = fullClass.replace(rexp, '').trim();
  return this;
}

/**
 * Get random number 
 * from 0 to firstParameter if second parameter don't set
 * of from firstParameter to Second parameter
 *
 * @param   {Number}   nFrom can be used as nTo if second parameter don't set
 * @param   {[Number]} nTo   
 * @returns {Number}   random number
 */
function random(nFrom, nTo) {
  if (nTo !== 0 && !nTo) {
    nTo = nFrom;
    nFrom = 0;
  }

  var rnd = Math.random();

  var retNumber = rnd * (nTo - nFrom) - nFrom;

  retNumber = Math.round(retNumber);

  retNumber = retNumber <= nFrom ? nFrom : retNumber;
  retNumber = retNumber >= nTo ? nTo : retNumber;

  return retNumber;
}

function vkReq(req, data, callback) {
  VK.api(req, data, function (resp) {
    if (resp.error) {
      console.log(resp);
      throw 'Error in ' + req + '!';
    }
    callback(resp);
  });
}

/**
 * return function by functionName. 
 * support for function's in object's
 *
 * @param   {String}   functionName   function name or path
 * @param   {Object}   [inObj=window] 
 * @returns {Function} 
 */
function getFnc(functionName, inObj) {
  if (typeof functionName === 'function') {
    return functionName;
  }

  inObj = inObj || window;

  if (functionName.indexOf('.') === -1) {
    var fRet = inObj[functionName] || function () {
      var err = 'ERROR: No function "';
      err += functionName.toString();
      err += '" in "';
      err += inObj.toString();
      err += '"!';
      console.log(err);
    };

    return fRet;
  } else {

    var arrWay = functionName.split('.');
    var firstInWay = arrWay.shift();

    var newObj = inObj[firstInWay];
    var newFName = arrWay.join('.');

    return getFnc(newFName, newObj);

  }

}

var vkFriends = o_o = function (opt) {
  return o_o.ini(opt);
};

o_o.option = {
  //Fields of user
  frndFields: 'first_name,photo_100,screen_name,sex,bdate',
  // instead empty space will shown random person, thet is not in app;
  showRandomPerson: true,
  //count of friends in one screen
  countOfFriendsInView: 7,
  //time for animation for changing list
  animMs: 700, //0.7s
  //className of disabled button
  clsButtonDisable: 'vkfButtonDisable',
  //id's of Buttons to manipulate list of friends
  idButtonUp: 'vkfButtonUp',
  idButtonDown: 'vkfButtonDown',
  //id of main friends holder 
  idFriendsPlace: 'vkfList',
  //id's of list holder 
  idHolderPrev: 'vkfHolderPre',
  idHolderNow: 'vkfHolderNow',
  idHolderNext: 'vkfHolderNext',
  //InnerHTML in preloadHolder
  preloadText: 'Friends loading...',
  //class, for temp holder, that created before request to VK
  clsPreloadHolder: 'vkfTempHolder',
  //class toggle for filter buttons
  clsFilterToogle: 'applied',
  //
  styleImgPreload: 'width:1px; height:1px; position:absolute; top:-100px',
  //Place (id of element), where will be placed ims's for preload background-image
  idImgLimb: '',
  // Attributes of current user. if undefined - script will get it from vk
  userAdd: true,
  scoreUserAlwaysFirst: true, //If false - userWillBe at first place everytime
  //object of user (can be generated on server)
  userObject: '',
  //Functions in actions
  funcAfterPreload: '',
  funcAfterFullLoad: '',
  //All about score
  scoreNeedToLoad: false,
  scoreRequestUrl: 'get_score.php',
  scoreRequestKey: 'friend',
  scoreNeedToSort: true,
  inviteMsg: 'Hello, $first_name! I will glad to see you here.',
  setOption: function (key, value) {
    if (typeof this[key] !== 'undefined') {
      this[key] = value;
      return true;
    }
  }
};

o_o.template = {
  reservedDomAttr: ['tag', 'child', 'click', 'bgimg', 'fncAddClass'],
  //Template for friend who is in app
  inApp: {
    'tag': 'div',
    'class': 'vkfFriend vkfInApp',
    'id': 'vkfBlock$inc',
    'child': [
      {
        'tag': 'div',
        'class': 'vkfName',
        'child': '$first_name'
      },
      {
        'tag': 'div',
        'class': 'vkfAva',
        'bgimg': '$photo_100'
      }
    ]
  },
  //Template for friend that is not in app
  notInApp: {
    'tag': 'div',
    'class': 'vkfFriend',
    'id': 'vkfBlock$inc',
    'child': [
      {
        'tag': 'div',
        'class': 'vkfName',
        'child': '$first_name'
      },
      {
        'tag': 'div',
        'class': 'vkfAva',
        'bgimg': '$photo_100'
      }
    ]
  },
  //Template for invite friend
  randomPerson: {
    'tag': 'div',
    'class': 'vkfFriend vkfInvite',
    'id': 'vkfBlock$inc',
    'child': {
      'tag': 'div',
      'class': 'vkfAva',
      'bgimg': '$photo_100',
      'click': 'o_o.vkf.inviteBox',
      'child': {
        'tag': 'span',
        'child': 'invite'
      }
    }

  },
  setTemplate: function (key, value) {
    if (typeof this[key] !== 'undefined') {
      this[key] = value;
      return true;
    }
  }
};
/**
 * constructor of template. take html, convert it to object
 * @param   {String} htm html, that need to convert
 * @returns {Object} 
 */
o_o.htmlToObj = function (htm) {
  //prepare html to convert it to dom
  var html = htm.trim().replace(/>\s+</g, '><');
  console.log(html);
  var tDom = document.createElement('div');
  tDom.innerHTML = html;

  var tObj = (function getObjFromDom(tNode) {

    if (tNode.nodeType === 3) { //TEXT_NODE
      return tNode.nodeValue;
    }

    var tObj = {
      tag: tNode.tagName
    };

    var attrs = tNode.attributes;

    for (var tKey = 0; tKey < attrs.length; tKey++) {
      var attrName = attrs[tKey].name;
      //spike for fncAddClass
      attrName =  attrName === 'fncaddclass' ? 'fncAddClass' : attrName;
      var attrVal = attrs[tKey].value;
      tObj[attrName] = attrVal;
    }


    if ( tNode.childElementCount > 1) {

      var childs = tNode.childNodes;
      tObj.child = [];

      for (var chldN = 0; chldN < childs.length; chldN++) {
        var objToAdd = getObjFromDom(childs[chldN]);
        tObj.child.push(objToAdd);
      }

    } else if (tNode.hasChildNodes()) {

      tObj.child = getObjFromDom(tNode.firstChild);
    
    } 

    return tObj;
  })(tDom.firstChild); //var tObj 

  return tObj;
};
//Friends list mover up and down @param {Boolean} isToUp
o_o.move = function (isToUp) {

  //Moving after (filter | load)
  if (typeof isToUp === 'undefined') {
    o_o.allFriends.nowFirstFrnd = -1;
    isToUp = true;
  }

  //Moving after (filter | load)
  if (typeof isToUp === 'string') {
    isToUp = isToUp === 'up';
  }

  //Return if already moving
  if (o_o.moveListOfFriendsIsBusy === true)
    return false;
  o_o.moveListOfFriendsIsBusy = true;

  //Return if it cant be moved
  if (iCantMove()) {
    console.log('i can\'t move');
    o_o.moveListOfFriendsIsBusy = false;
    return false;
  }
  o_o.ui.moveFriends(isToUp, o_o.allFriends.nowFirstFrnd === -1);
  o_o.ui.refreshButtons();

  function iCantMove() {
    var cantBeMoved = o_o.allFriends.cantBeMoved();
    return isToUp ? cantBeMoved.up : cantBeMoved.down;
  }
};

o_o.ui = {
  //Initialize buttons to move friends list up and down
  initialize: function () {
    this.friendsPlace = getElemByOption('idFriendsPlace');
    this.limb = o_o.option.idImgLimb ? getElemByOption('idImgLimb') : document.body;

    this.butUp = getElemByOption('idButtonUp');
    this.butDown = getElemByOption('idButtonDown');

    this.butUp.onclick = function () {
      o_o.move(true);
    };
    this.butDown.onclick = function () {
      o_o.move(false);
    };

    //Find optName in options and return dom with that id
    function getElemByOption(optName) {
      var optVal = o_o.option[optName];

      var toRet = document.getElementById(optVal);
      if (!toRet)
        throw 'Can\t get "' + optName + '". There is no object with id "' + optVal + '"!';

      return toRet;
    }

  },
  showPreloader: function () {
    var tHolder = document.createElement('div');

    tHolder.id = o_o.option.idHolderNow;
    classAdd.call(tHolder, o_o.option.clsPreloadHolder);
    tHolder.innerHTML = o_o.option.preloadText;

    this.friendsPlace.appendChild(tHolder);
  },
  refreshButtons: function (justMake) {

    //We just need to disable\enable both buttons
    if (justMake !== undefined) {

      if (justMake) {
        classRemove.call(this.butUp, o_o.option.clsButtonDisable);
        classRemove.call(this.butDown, o_o.option.clsButtonDisable);
      } else {
        classAdd.call(this.butUp, o_o.option.clsButtonDisable);
        classAdd.call(this.butDown, o_o.option.clsButtonDisable);
      }

    } else { //justMake === undefined

      var cantBeMoved = o_o.allFriends.cantBeMoved();

      //Is butUp need to be disabled
      if (cantBeMoved.up) {
        classAdd.call(this.butUp, o_o.option.clsButtonDisable);
      } else {
        classRemove.call(this.butUp, o_o.option.clsButtonDisable);
      }

      //Is butDown need to be disabled
      if (cantBeMoved.down) {
        classAdd.call(this.butDown, o_o.option.clsButtonDisable);
      } else {
        classRemove.call(this.butDown, o_o.option.clsButtonDisable);
      }
    }
  },
  moveFriends: function (isToUp, withoutAnimation) {
    var oldHolder = document.getElementById(o_o.option.idHolderNow);
    var newHolder = document.createElement('div');
    newHolder.id = isToUp ? o_o.option.idHolderNext : o_o.option.idHolderPrev;

    //1. Get array of new friends
    var arrOfFriendsToShow = o_o.allFriends.getListOfFrnd(isToUp);

    //2. Add it to new holder
    for (var i in arrOfFriendsToShow) {
      var newDom = arrOfFriendsToShow[i].generateNode(i);
      newHolder.appendChild(newDom);
    }

    //3. Append new holder with friends
    this.friendsPlace.appendChild(newHolder);

    if (withoutAnimation) {
      //4. toogle id's of holder
      toggleHolders();
      //5. remove old holder, and enable buttons   
      removeOldHolder();
    } else {

      var spikeWait = 100;

      //Spike for animation of change class name will start
      setTimeout(function () {
        //4. toogle id's of holder
        toggleHolders();
        setTimeout(function () {
          //5. remove old holder, and enable buttons 
          removeOldHolder();
        }, o_o.option.animMs - spikeWait);

      }, spikeWait); //end spike

    }

    function toggleHolders() {
      oldHolder.id = isToUp ? o_o.option.idHolderPrev : o_o.option.idHolderNext;
      newHolder.id = o_o.option.idHolderNow;
    }

    function removeOldHolder() {
      o_o.ui.friendsPlace.removeChild(oldHolder);
      o_o.ui.refreshButtons();
      o_o.moveListOfFriendsIsBusy = false;
    }

  }
};

//class for friend
o_o.Friend = function (vkFriendResp, isInApp) {

  vkFriendResp = vkFriendResp || vkFriendDefaults();

  for (var tAttribute in vkFriendResp)
    this[tAttribute] = vkFriendResp[tAttribute];

  this.canBeShown = this.id > 1;

  this.isInApp = !!isInApp;
  this.inc = 0;
  this.score = -1;

  /**
   * parsing and replace attributes in string
   * @param {type} template
   * @returns {unresolved}
   */
  this.parseText = function (template) {
    var toRet = template;

    if (toRet.indexOf('$') === -1)
      return toRet;

    var allKeys = Object.keys(this);

    for (var tKey in allKeys) {
      var sToReplace = new RegExp('\\$' + allKeys[tKey], 'i');
      toRet = toRet.replace(sToReplace, this[allKeys[tKey]]);
    }

    return toRet;
  };

  //generate userNode
  this.generateNode = function (increment) {
    var nodeTemplate = {};
    this.inc = increment;

    //Select template
    if (this.canBeShown) {
      nodeTemplate = this.isInApp ? o_o.template.inApp : o_o.template.notInApp;
    } else {
      nodeTemplate = o_o.template.randomPerson;
    }

    //spike for makeNode
    var me = this;

    return (function makeNode(nodeObj) {
      //If array - return NodeList
      if (nodeObj instanceof Array) {
        return processArray();
      }

      var tagName = nodeObj.tag || 'div';
      var result = document.createElement(tagName);

      //Set attributes
      for (var attrName in nodeObj) {
        if (o_o.template.reservedDomAttr.indexOf(attrName) === -1) {
          result.setAttribute(attrName, me.parseText(nodeObj[attrName]));
        }
      }

      if (nodeObj.child) {

        var childToAppend;

        if (typeof nodeObj.child === 'string') {
          childToAppend = makeTextNode(nodeObj.child);
        } else {
          childToAppend = makeNode(nodeObj.child);
        }

        result.appendChild(childToAppend);
      }

      //Set onclick action if defined
      if (nodeObj.click) {
        result.onclick = function (event) {

          event = event || window.event;

          //preventDefault with spike for IE<9
          if (event.preventDefault) {
            event.preventDefault();
          } else {
            event.returnValue = false;
          }

          //call the function of template
          return getFnc(nodeObj.click)(me, event);
        };
      }

      //Filter menu. add class if filter is applied
      if (nodeObj.fncAddClass) {

        var filterResult = getFnc(nodeObj.fncAddClass)(me);

        if (filterResult) {
          classAdd.call(result, filterResult);
        }

      }

      //Set background image, if defined and only after load;
      if (nodeObj.bgimg) {
        var timgNode = new Image();
        var limb = o_o.ui.limb;

        timgNode.setAttribute('style', o_o.option.styleImgPreload);

        timgNode.onload = function () {

          result.style.backgroundImage = 'url("' + this.src + '")';
          this.parentElement.removeChild(this);

        };

        timgNode.src = me.parseText(nodeObj.bgimg);

        limb.appendChild(timgNode);
      }
      return result;

      //if we get an array in template, this function will process it
      function processArray() {
        var tCont = document.createDocumentFragment();
        for (var tKey in nodeObj) {
          tCont.appendChild(makeNode(nodeObj[tKey]));
        }

        return tCont;
      }

      //generate text node by value
      function makeTextNode(txt) {
        txt = me.parseText(txt);
        var tNode = document.createTextNode(txt);
        return tNode;
      } // End of makeTextNode

    })(nodeTemplate); //End of makeNode

  }; //End of this.generateNode;

  function vkFriendDefaults() {
    var allDefault = {
      'id': '1',
      'nickname': 'Pratnomenis',
      'domain': 'pratnomenis',
      'sex': '1',
      'bdate': '01.01',
      'city': '0',
      'country': '0',
      'timezone': '',
      'photo_50': 'http://vk.com/images/camera_c.gif',
      'photo_100': 'http://vk.com/images/camera_b.gif',
      'photo_200_orig': 'http://vk.com/images/camera_a.gif',
      'has_mobile': '0',
      'score': '0'
    };

    var needVars = o_o.option.frndFields.split(",");
    var objToReturn = {};

    for (var ti in needVars) {
      var key = needVars[ti];
      var val = allDefault[key] || '';
      objToReturn[key] = val;
    }

    return objToReturn;
  }
};
//array of all Friend's
o_o.allFriends = {
  collection: [],
  //was nowFirstFrndIdInFilter
  push: function (something) {
    return this.collection.push(something);
  },
  sort: function (something) {
    return this.collection.sort(something);
  },
  sortByScore: function () {
    if (o_o.option.scoreNeedToSort) {
      if (o_o.option.scoreUserAlwaysFirst) {
        var usrRealScore = +this.collection[0].score;
        this.collection[0].score = 1 / 0;
      }

      this.sort(function (a, b) {
        return b.score - a.score;
      });

      if (o_o.option.scoreUserAlwaysFirst) {
        this.collection[0].score = usrRealScore;
      }

      this.slice.refresh();
    }
  },
  sortByInApp: function () {
    this.sort(function (a, b) {
      var a = +a.isInApp * -2;
      var b = +b.isInApp * 2;
      return a + b;
    });
    this.slice.refresh();
  },
  get length() {
    return this.collection.length;
  },
  slice: {
    getNew: function (arrName) {
      return this[arrName].slice();
    },
    refresh: function () {

      this.inApp = [];
      this.notInApp = [];
      this.canBeShown = [];
      this.forInvite = [];

      var collection = o_o.allFriends.collection;

      for (var i in collection) {
        var tFrnd = collection[i];

        this[tFrnd.isInApp ? 'inApp' : 'notInApp'].push(tFrnd);

        if (tFrnd.canBeShown) {
          this.canBeShown.push(tFrnd);
        }

        if (!tFrnd.isInApp) {
          this.forInvite.push(tFrnd);
        }
      }

    }
  },
  //  Append value to allFriends by user id
  appendByUID: function (uid, key, val) {
    for (var num in this.collection) {
      if (this.collection[num].id === +uid) {
        this.collection[num][key] = val;
        return true;
      }
    }
  },
  nowFirstFrnd: -1,
  cantBeMoved: function () {
    var ffId = this.nowFirstFrnd;

    var cbmDown = ffId === 0;

    var fpAfterSlideDown = ffId + o_o.option.countOfFriendsInView;
    var cbmUp = fpAfterSlideDown > this.slice.canBeShown.length;

    //if it's first list
    if (ffId === -1)
      cbmUp = false;

    return {
      up: cbmUp,
      down: cbmDown
    };
  },
  loadUser: function (clbFromLoadUser) {
    if (o_o.option.userObject) {

      addUserThenLoadAll(o_o.option.userObject, clbFromLoadUser);

    } else {

      vkReq('users.get', {
          fields: o_o.option.frndFields
        },
        function (resp) {
          addUserThenLoadAll(resp.response[0], clbFromLoadUser);
        }
      );

    }

    function addUserThenLoadAll(usrObj, clbFromAddUser) {
      var usr = new o_o.Friend(usrObj, true);
      o_o.allFriends.push(usr);
      o_o.allFriends.loadFriends(clbFromAddUser);
    }

  }, //loadUser: function
  loadFriends: function (clbFromLoadFrnd) {

    vkReq('friends.get', {
        fields: o_o.option.frndFields
      },
      function (allFriends) {

        vkReq('friends.getAppUsers', {},
          function (appUsers) {
            constructFriendsArray(allFriends, appUsers);

            o_o.allFriends.loadScore(clbFromLoadFrnd);
          });

      });

    //fill and sort the allFriends
    function constructFriendsArray(allFriends, appFriends) {
      var arrFrnd = allFriends.response.items;
      var appUsers = appFriends.response;

      for (var arrI in arrFrnd) {

        var tfrndObj = arrFrnd[arrI];
        var isInApp = ~appUsers.indexOf(arrFrnd[arrI].id);

        var tFrnd = new o_o.Friend(tfrndObj, isInApp);

        o_o.allFriends.push(tFrnd);
      }

      //Sort friends array. App users will be first.
      o_o.allFriends.sortByInApp();
    }
  },
  loadScore: function (clbFromScore) {
    //Unless need to load score do callback
    if (!o_o.option.scoreNeedToLoad) {

      clbFromScore();

    } else { // o_o.option.scoreNeedToLoad

      var inApp = this.slice.inApp;
      var needId = [];

      for (var i in inApp) {
        needId.push(inApp[i].id);
      }

      var reqUrl = o_o.option.scoreRequestUrl;
      var reqData = o_o.option.scoreRequestKey + '=' + needId.join(',');

      var reqResponce = function (responce) {

        if (!Array.isArray(responce)) {
          throw 'Something wrong with score! Please check does "' + reqUrl + '" works fine?';
        }

        for (var tKey in responce) {
          var tId = responce[tKey].id || responce[tKey].uid;
          var tLevel = responce[tKey].level;

          o_o.allFriends.appendByUID(tId, 'score', tLevel);
        }

        o_o.allFriends.sortByScore();

        clbFromScore();
      };

      //Send request for friends score
      httpPost(reqUrl, reqData, reqResponce);
    }
  },
  getListOfFrnd: function (durationNext) {

    var needLengthArrToRet = o_o.option.countOfFriendsInView;

    if (this.nowFirstFrnd === -1) {
      this.nowFirstFrnd = 0;
    } else {
      this.nowFirstFrnd += needLengthArrToRet * (durationNext ? 1 : -1);
    }
    var numFrndToCheck = this.nowFirstFrnd;


    var cbsCollection = this.slice.canBeShown;
    var cbsCount = cbsCollection.length;

    var arrToRet = [];
    for (var i = 0; i < needLengthArrToRet; i++, numFrndToCheck++) {
      if (numFrndToCheck < cbsCount) {
        var tr = cbsCollection[numFrndToCheck];
        arrToRet.push(tr);
      } else {
        arrToRet = getMorePersonsList(arrToRet);
        break;
      }
    }

    return arrToRet;

    //return some random persons
    function getMorePersonsList(arrToRet) {

      if (o_o.option.showRandomPerson) {
        var arrNotShown = o_o.allFriends.slice.getNew('forInvite');

        while (arrNotShown.length > 0 && arrToRet.length < needLengthArrToRet) {

          var notShownLen = arrNotShown.length;
          var randomId = random(notShownLen);
          var frnd = arrNotShown.splice(randomId, 1)[0];

          if (frnd && arrToRet.indexOf(frnd) === -1)
            arrToRet.push(frnd);

        }

      }

      //Add empty users
      var alreadyLength = arrToRet.length;
      var emptyCount = needLengthArrToRet - alreadyLength;
      for (var ti = 0; ti < emptyCount; ti++) {
        arrToRet.push(new o_o.Friend());
      }

      return arrToRet;
    } //getMorePersonsList()

  }
};
//initialization
o_o.ini = function (options) {
  //Count of friends after filter;
  calculateOptions();

  try {
    o_o.ui.initialize();
    o_o.ui.showPreloader();

    if (o_o.option.funcAfterPreload) {
      getFnc(o_o.option.funcAfterPreload)();
    }

    var afterFullLoad = function () {
      o_o.move();
      if (o_o.option.funcAfterFullLoad) {
        getFnc(o_o.option.funcAfterFullLoad)();
      }
    };

    if (o_o.option.userAdd) {
      o_o.allFriends.loadUser(afterFullLoad);
    } else {
      o_o.allFriends.loadFriends(afterFullLoad);
    }

  } catch (e) {
    console.log(e);
  }

  function calculateOptions() {

    for (var tKey in options) {
      var tVal = options[tKey];
      var setOption = o_o.option.setOption(tKey, tVal);
      var setTemplate = o_o.template.setTemplate(tKey, tVal);

      if (setOption === setTemplate)
        throw "Undefined option " + tKey;

    }

  }

};
//filters
o_o.filter = new function () {

  //constructor
  var Filter = function (reqStr, matchStr) {

    this.reqStr = reqStr;
    this.matchStr = matchStr;

    this.hang = function (obj) {

      var domObj = typeof obj === 'string' ? document.getElementById(obj) : obj;
      var fltr = this;

      domObj.onclick = function (e) {
        e.preventDefault();

        var toggleClass = o_o.option.clsFilterToogle;

        if (this.className.indexOf(toggleClass) === -1) {

          fltr.hide();
          classAdd.call(this, toggleClass);

        } else {

          fltr.show();
          classRemove.call(this, toggleClass);

        }
      };

    };

    this.hide = doFilter.bind(this, true);
    this.show = doFilter.bind(this, false);

    //
    function doFilter(toHide) {

      var filter = this.reqStr;
      var matcher = this.matchStr;
      var filterLength = matcher.length;

      var arrFrnd = o_o.allFriends.collection;

      for (var t in arrFrnd) {
        var tFilterResult = arrFrnd[t]
          .parseText(filter)
          .substring(0, filterLength);

        if (matcher === tFilterResult)
          arrFrnd[t].canBeShown = !toHide;

      }

      o_o.allFriends.slice.refresh();
      o_o.move();
    }

  };

  this.add = function (name, reqStr, matchStr) {
    this[name] = new Filter(reqStr, matchStr);
  };

  this.add('inApp', '$isInApp', 'true');
  this.add('notInApp', '$isInApp', 'false');
  this.add('female', '$sex', '1');
  this.add('male', '$sex', '2');

  var vkToday = (function (tDate) {
    var d = tDate.getDay();
    var m = tDate.getMonth();
    var dd = d <= 9 ? '0' + d : d;
    var mm = m <= 9 ? '0' + m : m;
    return '' + dd + '.' + mm;
  })(new Date());
  //Today birthday
  this.add('birthday', '$bdate', vkToday);

  // Show all friends
  this.showAll = function () {
    filterBool(true);
  };

  //Hide all friends
  this.hideAll = function () {
    filterBool(false);
  };

  function filterBool(setVal) {
    var arrFrnd = o_o.allFriends.collection;

    for (var t in arrFrnd) {
      arrFrnd[t].canBeShown = setVal;
    }

    o_o.allFriends.slice.refresh();
    o_o.move();
  }

};

o_o.vkf = {
  inviteBox: function () {
    VK.callMethod("showInviteBox");
  },
  /**
   * invite friend by send showRequestBox
   * @param {Friend} frnd friend object
   */
  inviteMe: function (frnd) {

    var uid = frnd.id;
    var mess = o_o.option.inviteMsg;

    if (uid === 1) {

      o_o.vkf.inviteBox();

    } else {

      var t = 'my_key';
      mess = frnd.parseText(mess);

      VK.callMethod("showRequestBox", uid, mess, t);

    }
  }
};