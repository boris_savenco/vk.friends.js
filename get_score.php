<?php

header('Content-Type: application/json; charset=utf-8');
define('VKF_REQUEST_SCORE_KEY', 'friend'); //option: scoreRequestKey

$postUid = filter_input(INPUT_POST, VKF_REQUEST_SCORE_KEY);

/* * ****************
 * TEST MODE
 */

$arrUids = explode(',', $postUid);
$arrToRet = Array();

//add random score 4 friend
foreach ($arrUids as $tuid) {
  
  $arrToRet[] = Array(
      'level' => rand(1,500),
      'uid' => $tuid
  );
  
}

/*
 * #TEST MODE
 * ***************** */

/* * ****************
 * NORMAL MODE
 * 
 * Request to vk.com
 *
 */
/* Define APP_ID and APP_SECRET */
//require_once 'config/vk.php'; 
//
/*  @link http://vk.com/developers.php   @autor Oleg Illarionov */
//require_once 'lib/vkapi.class.php';
//
//$VK = new vkapi(APP_ID, APP_SECRET);
//$respVk = $VK->api(
//                      'secure.getUserLevel', 
//                      array('user_ids' => $postUid)
//                    );
//
//$arrToRet = $resp['response'];
/*
 * #NORMAL MODE  
 * ****************** */
//Return result as JSON
echo json_encode($arrToRet);
